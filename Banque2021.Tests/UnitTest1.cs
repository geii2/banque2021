using Xunit;
using LuhnNet;

namespace Banque2021.Tests;

public class UnitTest1
{
  [Fact]
  public void TestLuhn()
  {
    Client client1 = new Client("Albert", "Einstein", 44);

    Compte compte1 = client1.OuvrirCompte();

    Assert.True(Luhn.IsValid(compte1.numeroCompte));
  }

  [Fact]
  public void TestOpérations()
  {
    Client client1 = new Client("Albert", "Einstein", 44);

    Compte compte1 = client1.OuvrirCompte();

    compte1.Deposer(50);
    compte1.Retirer(10);

    Assert.Equal<float>(40, compte1.solde);
  }
}
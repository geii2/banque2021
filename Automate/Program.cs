﻿
[Menu("Client")]
public class ClientController
{
  [Menu("Créer", Position=1)]
  public static void Créer()
  {
    Console.Write("Le prénom : ");
    string Prénom = Console.ReadLine();

    Console.Write("Le nom : ");
    string Nom = Console.ReadLine();

    //Client client = new Client(Prénom, Nom);
  }

  [Menu("Lister", Position=2)]
  public static void Lister()
  {
 
  }
 
  [Menu("Exporter", Position=3)]
  public static void Exporter()
  {
 
  }

  [Menu("Importer", Position=4)]
  public static void Importer()
  {
 
  }
}

public class Program
{
  static void ActionRetirer(Compte compte, float montant)
  {
    Compte.CodeRetour resultat = compte.Retirer(montant);
    
    switch(resultat) {
      case  Compte.CodeRetour.Ok:
        Console.WriteLine("Opération effectuée avec succès");
        break;
      
      case Compte.CodeRetour.SoldeInsuffisant:
        Console.WriteLine("Le solde est insuffisant pour retirer");
        break;

      case Compte.CodeRetour.PlafondDepasse:
        Console.WriteLine("Plafond de retrait dépassé");
        break;
    }
  }

  static void Main()
  {
    MenuController.Show(typeof(ClientController));

    // See https://aka.ms/new-console-template for more information
    Client client1 = new Client("Einstein", "Albert", 25);
        
    Compte compte1 = client1.OuvrirCompte();
    compte1.Deposer(5.0F);

    CompteJeune.DepotMaxJeune = 120.0F; // static Commun à TOUS les CompteJeune pas unique le compte d'Albert
    // CompteJeune.RetraitMaxJeune = 20.0F; Impossible de modifer c'est un constante const

    ActionRetirer(compte1, 8.0F);
    ActionRetirer(compte1, 2.0F);
    Console.WriteLine(compte1);
    compte1.AfficherHistorique();
  }
}

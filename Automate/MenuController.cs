using System.Reflection;

public class MenuAttribute: Attribute
{
  public string Titre { get; private set; }

  public int Position { get; set; }

  public string MethodName { get; set; }

  public MenuAttribute(string Titre)
  {
    this.Titre = Titre;
  }
}

static class MenuController
{
  static public void Show(Type Classe) 
  {
    string Titre;

    SortedList<int, MenuAttribute> Liste = new SortedList<int, MenuAttribute>();

    System.Attribute[] attrs = System.Attribute.GetCustomAttributes(Classe);  

    foreach (System.Attribute attr in attrs)  
    {  
      if (attr is MenuAttribute)  
      { 
        Titre = ((MenuAttribute)attr).Titre;
      }
    }

    MethodInfo[] methods = Classe.GetMethods(BindingFlags.Public | BindingFlags.Static);

    foreach (var method in methods) 
    {  
      attrs = System.Attribute.GetCustomAttributes(method);  

      foreach (System.Attribute attr in attrs)  
      {  
        if (attr is MenuAttribute)  
        { 
          MenuAttribute attribute = (MenuAttribute)attr;
          Liste.Add(attribute.Position, attribute);
          //method?.Invoke(null, null);
        }
      }
    }

    Console.Clear();
    Console.WriteLine("┌─────────────────────────────────────────┐");

    foreach(KeyValuePair<int, MenuAttribute> kvp in Liste)
    {
      string line = $"{kvp.Value.Position} {kvp.Value.Titre}";
      Console.WriteLine("│ "+line.PadRight(40, ' ')+'│');
    }
    
    Console.WriteLine("└─────────────────────────────────────────┘");

    ConsoleKeyInfo c = Console.ReadKey(true);
  }
}
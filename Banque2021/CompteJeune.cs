public class CompteJeune : Compte
{
	public const float RetraitMaxJeune = 10.0F;  // Constantes
	public static float DepotMaxJeune = 100.0F;  // static Variable commune à tous les comptes jeunes
	
	public CompteJeune(Client client)
		:base(client)
	{ }
	
	public override CodeRetour Retirer(float Montant)
	{
		if (Montant > RetraitMaxJeune)
		{
			return CodeRetour.PlafondDepasse;
		}
		
		return base.Retirer(Montant);
	}
	
	public override CodeRetour Deposer(float Montant)
	{
	  if (Montant < DepotMaxJeune)
		{
			return CodeRetour.PlafondDepasse;
		}

		return base.Deposer(Montant);
	}
	
	public override string ToString() {
		return $"N° de compte jeune {numeroCompte} votre solde est {solde}";
	}
}

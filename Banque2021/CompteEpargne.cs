public class CompteEpargne : Compte
{
	public float tauxInteret { get; private set; }
	
	public CompteEpargne(Client client, float tauxInteret)
		:base(client)
	{
		this.tauxInteret = tauxInteret;
	}
}

using LuhnNet;

public class Compte
{	
	// Enumération = liste de constantes avec compteur automatique
	public enum CodeRetour {
    Ok,
	  SoldeInsuffisant,
    MontantInvalide,
    PlafondDepasse
	}

  private Client Client;

  public string numeroCompte { get; private set; }

  public float solde { get; private set; }

  List<Opération> historique;

	public Compte(Client client)
	{
		Random generateur = new Random();

		this.Client = client;
		this.numeroCompte = generateur.Next(10000).ToString("D4");
		this.numeroCompte +=Luhn.CalculateCheckDigit(numeroCompte);
		

		this.solde = 0;
    historique = new List<Opération>();
	}
	
	public virtual CodeRetour Deposer(float Montant)
	{
		if (Montant <= 0.0F)
		{
			return CodeRetour.MontantInvalide;
		}

		solde += Montant;
    historique.Add(new Opération(Montant, "Dépôt"));
		return CodeRetour.Ok;
}
	
	public virtual CodeRetour Retirer(float Montant)
	{
		if (Montant <= 0.0F)
		{
			return CodeRetour.MontantInvalide;
		}

		if (Montant > solde)
		{
			return CodeRetour.SoldeInsuffisant;
		}

    historique.Add(new Opération(Montant, "Retrait"));
		solde -= Montant;
		return CodeRetour.Ok;
	}
	
  public void AfficherHistorique() {
    foreach (Opération ligne in historique)
    {
      Console.WriteLine($"{ligne.DateHeure} : {ligne.Action} de {ligne.Montant} euros");
    }
  }

	public override string ToString() {
		return $"N° de compte {numeroCompte} votre solde est {solde}";
	}
}

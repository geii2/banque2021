class Opération
{
  public float Montant { get; private set; }

  public DateTime DateHeure { get; private set; }

  public string Action { get; private set; }

  public Opération(float Montant, string Action)
  {
    this.Montant = Montant;
    this.Action = Action;
    DateHeure = DateTime.Now;  // Date et Heure courante par défaut
  }
}
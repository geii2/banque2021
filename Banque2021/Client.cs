public class Client 
{
	public string Nom { get; private set; }
	public string Prenom { get; private set; }
	public int Age { get; private set; }
	
  List<Compte> Comptes;

	public Client(string Nom, string Prenom, int Age)
	{
		this.Nom = Nom;
		this.Prenom = Prenom;
		this.Age = Age;
    this.Comptes = new List<Compte>();
	}
	
	public Compte OuvrirCompte() {
		
		if (Age < 18)
		{
      CompteJeune compte = new CompteJeune(this);
      Comptes.Add(compte);
			return compte;
		}
		else
		{
      Compte compte = new Compte(this);
      Comptes.Add(compte);
			return compte; 
		}
	}
}

public class CompteCredit : Compte
{
	public float tauxInteret { get; private set; }
	public float Reserve { get; private set; }
	
	public CompteCredit(Client client, float tauxInteret, float Reserve)
		:base(client)
	{ 
		this.tauxInteret = tauxInteret;
		this.Reserve = Reserve;
	}
}
